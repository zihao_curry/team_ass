/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Entities;

/**
 *
 * @author asus
 */

import java.io.Serializable;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Column;
import jakarta.persistence.Table;



@Entity
@Table(name = "JOB")
public class job implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "JOBID")
    private int JOBID;
    
    

    @Column(name = "PROVIDERID")
    private int PROVIDERID;
    
    @Column(name = "TITLE")
    private String TITLE;
    
    @Column(name = "KEYWORDS")
    private String KEYWORDS;
    
    @Column(name = "DESCRIPTION")
    private String DESCRIPTION;
    
    @Column(name = "PAYMENT_OFFER")
    private double PAYMENT_OFFER;
    
    @Column(name = "STATUS")
    private String STATUS;

    public int getJOBID() {
        return JOBID;
    }

    public int getPROVIDERID() {
        return PROVIDERID;
    }

    public String getTITLE() {
        return TITLE;
    }

    public String getKEYWORDS() {
        return KEYWORDS;
    }

    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public double getPAYMENT_OFFER() {
        return PAYMENT_OFFER;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setJOBID(int JOBID) {
        this.JOBID = JOBID;
    }

    public void setPROVIDERID(int PROVIDERID) {
        this.PROVIDERID = PROVIDERID;
    }

    public void setTITLE(String TITLE) {
        this.TITLE = TITLE;
    }

    public void setKEYWORDS(String KEYWORDS) {
        this.KEYWORDS = KEYWORDS;
    }

    public void setDESCRIPTION(String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }

    public void setPAYMENT_OFFER(double PAYMENT_OFFER) {
        this.PAYMENT_OFFER = PAYMENT_OFFER;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }
    
    

}

