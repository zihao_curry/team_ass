/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Entities;

/**
 *
 * @author 26625
 */
import java.io.Serializable;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Column;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;


@Entity
@Table(name = "JOBAPPLICATION")
public class jobapplication implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "APPLICATIONID")
    private int APPLICATIONID;
    
    @Column(name = "JOBID")
    private int JOBID;
    
    
    @Column(name = "FREELANCERID")
    private int FREELANCERID;
    
    @Column(name = "STATUS")
    private String STATUS;
    
    
        @Transient
    private String freelancerMessage;

    public int getAPPLICATIONID() {
        return APPLICATIONID;
    }

    public void setAPPLICATIONID(int APPLICATIONID) {
        this.APPLICATIONID = APPLICATIONID;
    }

    public int getJOBID() {
        return JOBID;
    }

    public void setJOBID(int JOBID) {
        this.JOBID = JOBID;
    }

    public int getFREELANCERID() {
        return FREELANCERID;
    }

    public void setFREELANCERID(int FREELANCERID) {
        this.FREELANCERID = FREELANCERID;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }


    


    public String getFreelancerMessage() {
        return freelancerMessage;
    }

    public void setFreelancerMessage(String freelancerMessage) {
        this.freelancerMessage = freelancerMessage;
    }
    
    
}
