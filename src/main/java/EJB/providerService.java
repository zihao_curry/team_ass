/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/J2EE/EJB30/StatelessEjbClass.java to edit this template
 */
package EJB;

import Entities.freelancer;
import jakarta.ejb.Stateless;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.EntityManager;
import Entities.job;
import Entities.jobapplication;
import Entities.provider;
import jakarta.inject.Provider;
import jakarta.persistence.Query;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;

/**
 *
 * @author asus
 */
@Stateless
public class providerService implements providerServiceLocal {
    @PersistenceContext(unitName = "Junnan_LDS_war_1.0PU")
    private EntityManager em;
    
    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    
    public providerService(){
    }
    
    public void persist(Object object) {
        try {
            em.persist(object);
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", e);
            throw new RuntimeException(e);
        }
    }
    
    @Override
    public void createJob(String title, String description,String email, String status, String keywords, Double payment_offer) {
        int id = (int) em.createQuery("SELECT p.PROVIDERID FROM provider p WHERE p.EMAIL = :email")
                .setParameter("email", email)
                .getSingleResult();
        job job = new job();
        job.setTITLE(title);
        job.setDESCRIPTION(description);
        job.setKEYWORDS(keywords);
        job.setPAYMENT_OFFER(payment_offer);
        job.setSTATUS(status);
        job.setPROVIDERID(id);
        em.persist(job);
    }
    
    @Override
    public List<job> getJobsByProvider(String email) {
        int providerId = (int) em.createQuery("SELECT p.PROVIDERID FROM provider p WHERE p.EMAIL = :email")
                .setParameter("email", email)
                .getSingleResult();
        return em.createQuery("SELECT j FROM job j WHERE j.PROVIDERID = :providerId")
                .setParameter("providerId", providerId)
                .getResultList();
    }
    
    @Override
    public void revokeJob(int jobId) {
        job jobToDelete = em.find(job.class, jobId);
        if (jobToDelete != null) {
            em.remove(jobToDelete);
        }
    }
    
    @Override
    public void deleteProviderByEmail(String email) {
        provider provider = (provider) em.createQuery("SELECT p FROM provider p WHERE p.EMAIL = :email", Provider.class)
                .setParameter("email", email)
                .getSingleResult();
        em.remove(provider);
    }
    
    @Override
    public provider findproviderByEmail(String email){
        provider provider = (provider) em.createQuery("SELECT p FROM provider p WHERE p.EMAIL = :email", Provider.class)
                .setParameter("email", email)
                .getSingleResult();
        return provider;
        
    }
    
    


@Override
public List<jobapplication> getRelatedJobApplicationsWithMessage(int providerId) {
    List<Object[]> resultList = em.createQuery("SELECT ja, f.MESSAGE FROM jobapplication ja, job j, freelancer f WHERE j.JOBID = ja.JOBID AND j.PROVIDERID = :providerId AND ja.FREELANCERID = f.FREELANCERID")
            .setParameter("providerId", providerId)
            .getResultList();
    
    List<jobapplication> jobApplications = new ArrayList<>();
    for (Object[] result : resultList) {
        jobapplication ja = (jobapplication) result[0];
        String freelancerMessage = (String) result[1];
        ja.setFreelancerMessage(freelancerMessage);
        jobApplications.add(ja);
    }
    
    return jobApplications;
}


    
    
    @Override
    public void acceptapplications(int freelancerid, int jobid){
        jobapplication jobapp = (jobapplication) em.createQuery("SELECT ja FROM jobapplication ja WHERE ja.FREELANCERID = :freelancerid AND ja.JOBID = :jobid")
                .setParameter("freelancerid", freelancerid)
                .setParameter("jobid", jobid)
                .getSingleResult();
        jobapp.setSTATUS("accepted");
        em.merge(jobapp);
        job job = (job) em.createQuery("SELECT j FROM job j WHERE j.JOBID = :jobid ")
                .setParameter("jobid", jobid)
                .getSingleResult();
        job.setSTATUS("closed");
        em.merge(job);
    }
    
    @Override
    public void completeapplications( int jobid, double payment, int selectedFreelancerId){
        job job = (job) em.createQuery("SELECT j FROM job j WHERE j.JOBID = :jobid ")
                .setParameter("jobid", jobid)
                .getSingleResult();
        job.setSTATUS("completed");
        em.merge(job);
        freelancer f = (freelancer) em.createQuery("SELECT f FROM freelancer f WHERE f.FREELANCERID = :selectedFreelancerId")
                .setParameter("selectedFreelancerId", selectedFreelancerId)
                .getSingleResult();
        double account = f.getPAYMENT_ACCOUNT();
        f.setPAYMENT_ACCOUNT(payment+account);
        em.merge(f);
    }



}
