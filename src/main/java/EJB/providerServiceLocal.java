/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/J2EE/EJB30/SessionLocal.java to edit this template
 */
package EJB;

import jakarta.ejb.Local;
import java.util.List;
import Entities.job;
import Entities.jobapplication;
import Entities.provider;

/**
 *
 * @author asus
 */
@Local
public interface providerServiceLocal {
    void createJob(String title, String description, String email, String status, String keywords, Double payment_offer);
    List<job> getJobsByProvider(String email);
    void revokeJob(int jobId);
    void deleteProviderByEmail(String email);
    provider findproviderByEmail(String email);
    List<jobapplication> getRelatedJobApplicationsWithMessage(int providerId);
    void acceptapplications(int freelancerid, int jobid);
    void completeapplications( int jobid, double payment, int selectedFreelancerId);
    
}
