package EJB;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.NoResultException;
import Entities.freelancer;
import Entities.job;
import Entities.jobapplication;
import jakarta.ejb.Stateless;
import jakarta.persistence.*;
import jakarta.transaction.Transactional;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Stateless
public class freelancerService implements freelancerServiceLocal {

    @PersistenceContext(unitName = "Junnan_LDS_war_1.0PU")
    private EntityManager em;

    public freelancerService() {
    }

    public void persist(Object object) {
        try {
            em.persist(object);
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public freelancer findfreelancerByEmail(String email) {
        try {
            freelancer f = (freelancer) em.createQuery("SELECT f FROM freelancer f WHERE f.EMAIL = :email")
                    .setParameter("email", email)
                    .getSingleResult();
            return f;
        } catch (NoResultException e) {

            return null;
        }
    }

    @Override
    public void updateFreelancer(freelancer freelancer) {
        em.merge(freelancer);
    }

    @Override
    @Transactional
    public void deleteFreelancerByEmail(String email) {
        try {
            freelancer freelancer = (freelancer) em.createQuery("SELECT f FROM freelancer f WHERE f.EMAIL = :email")
                    .setParameter("email", email)
                    .getSingleResult();
            if (freelancer != null) {
                em.remove(freelancer);
            } else {
                System.out.println("Freelancer not found for email: " + email);
            }
        } catch (NoResultException e) {
            System.out.println("No freelancer found for email: " + email);
        } catch (Exception e) {
            System.out.println("Error deleting freelancer: " + e.getMessage());
        }
    }
    
    @Override
    public List<job> freelancershowalljobs(){
        List<job> j = (List<job>) em.createQuery("SELECT j FROM job j")
                .getResultList();
        return j;
    }
    
    @Override 
    public List<job> searchbyid(int id){
        List<job> j = (List<job>) em.createQuery("SELECT j FROM job j WHERE j.JOBID = :id")
                .setParameter("id", id)
                .getResultList();
        return j;
    }
    
    @Override 
    public List<job> searchbykeywords(String keywords){
        List<job> j = (List<job>) em.createQuery("SELECT j FROM job j WHERE j.KEYWORDS = :keywords")
                .setParameter("keywords", keywords)
                .getResultList();
        return j;
    }
    
    @Override
    public List<job> freelancerallapplications(String email){
        freelancer f = (freelancer) em.createQuery("SELECT f FROM freelancer f WHERE f.EMAIL = :email")
                .setParameter("email", email)
                .getSingleResult();
        List<job> j = (List<job>) em.createQuery("SELECT ja FROM jobapplication ja WHERE ja.FREELANCERID = :freelancerId")
                .setParameter("freelancerId", f.getFREELANCERID())
                .getResultList();
        return j;
    }
    
    @Override
    public void freelancerapplyjobs(int jobid,int fid){
        jobapplication app = new jobapplication();
        app.setJOBID(jobid);
        app.setFREELANCERID(fid);
        app.setSTATUS("pending");
        em.persist(app);
    }
    
    @Override
    public void deleteApplication(int freelancerId, int jobId) {
        Query query = em.createQuery("DELETE FROM jobapplication ja WHERE ja.FREELANCERID = :freelancerId AND ja.JOBID = :jobId")
                .setParameter("freelancerId", freelancerId)
                .setParameter("jobId", jobId);
        query.executeUpdate();
}
    
    


}
