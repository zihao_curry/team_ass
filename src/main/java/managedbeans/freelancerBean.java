/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSF/JSFManagedBean.java to edit this template
 */
package managedbeans;

import EJB.freelancerServiceLocal;
import Entities.freelancer;
import jakarta.ejb.EJB;
import jakarta.inject.Named;
import jakarta.enterprise.context.SessionScoped;
import jakarta.inject.Inject;
import java.io.Serializable;
import Entities.job;
import java.util.List;
import EJB.providerServiceLocal;

/**
 *
 * @author 26625
 */
@Named(value = "freelancerBean")
@SessionScoped
public class freelancerBean implements Serializable{
    
    private String email;
    private String name;
    private String skills;
    private String message;
    private String freelancername;
    private String freelanceremail;
    private String freelancerskills;
    private String freelancermessage;
    private int freelancerid;
    
    private int ID;
    private String Keywords;
    private int selectedJobId;
    
    @EJB
    @Inject
    private freelancerServiceLocal fsl;
    
    @Inject
    private providerServiceLocal psl;

    
    /**
     * Creates a new instance of freelancerBean
     */
    public freelancerBean() {
    }
    
    
    public String showfreelancername(){
        freelancer f1 = fsl.findfreelancerByEmail(email);
        freelancername = f1.getNAME();
        return freelancername;
    }
    
    public String showfreelanceremail(){
        freelancer f2 = fsl.findfreelancerByEmail(email);
        freelanceremail = f2.getEMAIL();
        return freelanceremail;
    }

    public int showfreelancerid(){
        freelancer f3 = fsl.findfreelancerByEmail(email);
        freelancerid = f3.getFREELANCERID();
        return freelancerid;
    }
    
    public String showfreelancerskills(){
        freelancer f4 = fsl.findfreelancerByEmail(email);
        freelancerskills = f4.getSKILLS();
        return freelancerskills;
    }
    
    public String showfreelancermessage(){
        freelancer f5 = fsl.findfreelancerByEmail(email);
        freelancermessage = f5.getMESSAGE();
        return freelancermessage;
    }
    
    public void updateFreelancerInfo(){
        freelancer f = fsl.findfreelancerByEmail(email);
        if (f != null){
            f.setMESSAGE(message);
            f.setSKILLS(skills);
            if (!"".equals(name)){
                f.setNAME(name);
            }
            fsl.updateFreelancer(f);
        }
    }  
    
    public List<job> showalljobs(){
        List<job> j = fsl.freelancershowalljobs();
        return j;
    }
    
    public List<job> searchbyid(){
        List<job> j = fsl.searchbyid(ID);
        return j;
    }

    public List<job> searchbykeywords(){
        List<job> j = fsl.searchbykeywords(Keywords);
        return j;
    }
    
    
    public void saveSelectedJobId(int jobId) {
        setSelectedJobId(jobId);
    }
    
    public double getFreelancerPaymentAccount() {
    freelancer f = fsl.findfreelancerByEmail(email);
    return f.getPAYMENT_ACCOUNT();
}
    
    public void applyjob() {
        freelancer f = fsl.findfreelancerByEmail(email);
        fsl.freelancerapplyjobs(selectedJobId,f.getFREELANCERID());
    }
    
    public List<job> applications(){
        List<job> j = fsl.freelancerallapplications(email);
        return j;
    }
    
    public void revokeapp(){
        freelancer f = fsl.findfreelancerByEmail(email);
        fsl.deleteApplication(f.getFREELANCERID(), selectedJobId);
    }
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSkills() {
        return skills;
    }

    public void setSkills(String skills) {
        this.skills = skills;
    }

    public String getMessage() {
        return message;
    }

    public String getFreelancername() {
        return freelancername;
    }

    public void setFreelancername(String freelancername) {
        this.freelancername = freelancername;
    }

    public String getFreelanceremail() {
        return freelanceremail;
    }

    public void setFreelanceremail(String freelanceremail) {
        this.freelanceremail = freelanceremail;
    }

    public String getFreelancerskills() {
        return freelancerskills;
    }

    public void setFreelancerskills(String freelancerskills) {
        this.freelancerskills = freelancerskills;
    }

    public String getFreelancermessage() {
        return freelancermessage;
    }

    public void setFreelancermessage(String freelancermessage) {
        this.freelancermessage = freelancermessage;
    }

    public int getFreelancerid() {
        return freelancerid;
    }

    public void setFreelancerid(int freelancerid) {
        this.freelancerid = freelancerid;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getKeywords() {
        return Keywords;
    }

    public void setKeywords(String Keywords) {
        this.Keywords = Keywords;
    }

    public freelancerServiceLocal getFsl() {
        return fsl;
    }

    public void setFsl(freelancerServiceLocal fsl) {
        this.fsl = fsl;
    }

    
    public void setMessage(String message) {
        this.message = message;
    }

    public int getSelectedJobId() {
        return selectedJobId;
    }

    public void setSelectedJobId(int selectedJobId) {
        this.selectedJobId = selectedJobId;
    }






    
    
}

