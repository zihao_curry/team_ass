package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Setup {

    private static final String URL = "jdbc:derby://localhost:1527/DataB;create=true";
    private static final String USER = "db";
    private static final String PASSWD = "db";

    public static void main(String[] args) {

        // Create database connection
        try (Connection conn = DriverManager.getConnection(URL, USER, PASSWD)) {
            System.out.println("Database connection established.");

            // Create tables
            createFreelancerTable(conn);
            createProviderTable(conn);
            createJobTable(conn);
            createJobApplicationTable(conn);
            createAdministratorTable(conn);

            System.out.println("Tables created successfully.");
        } catch (SQLException ex) {
            System.err.println("An error occurred while creating the tables: " + ex.getMessage());
        }
    }

    private static void createFreelancerTable(Connection conn) throws SQLException {
        String sql = "CREATE TABLE Freelancer ("
                + "  FreelancerID INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,"
                + "  Name VARCHAR(255) NOT NULL,"
                + "  Email VARCHAR(255) NOT NULL UNIQUE,"
                + "  Password VARCHAR(255) NOT NULL,"
                + "  Skills VARCHAR(1024),"
                + "  Message VARCHAR(500),"
                + "  Payment_Account DECIMAL(10,2)"
                + ")";
        try (Statement stmt = conn.createStatement()) {
            stmt.executeUpdate(sql);
        }
    }

    private static void createProviderTable(Connection conn) throws SQLException {
        String sql = "CREATE TABLE Provider ("
                + "  ProviderID INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,"
                + "  Name VARCHAR(255) NOT NULL,"
                + "  Email VARCHAR(255) NOT NULL UNIQUE,"
                + "  Password VARCHAR(255) NOT NULL"
                + ")";
        try (Statement stmt = conn.createStatement()) {
            stmt.executeUpdate(sql);
        }
    }

    private static void createJobTable(Connection conn) throws SQLException {
        String sql = "CREATE TABLE Job ("
                + "  JobID INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,"
                + "  Title VARCHAR(255) NOT NULL,"
                + "  Keywords VARCHAR(255),"
                + "  Description VARCHAR(500) NOT NULL,"
                + "  Payment_Offer DECIMAL(10,2) NOT NULL,"
                + "  Status VARCHAR(20) CHECK (Status IN ('open', 'closed', 'completed')) DEFAULT 'open',"
                + "  ProviderID INT,"
                + "  FOREIGN KEY (ProviderID) REFERENCES Provider(ProviderID)"
                + ")";
        try (Statement stmt = conn.createStatement()) {
            stmt.executeUpdate(sql);
        }
    }

    private static void createJobApplicationTable(Connection conn) throws SQLException {
    String sql = "CREATE TABLE JobApplication ("
            + "  ApplicationID INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,"
            + "  JobID INT,"
            + "  FreelancerID INT,"
            + "  Status VARCHAR(20) CHECK (Status IN ('pending', 'accepted', 'rejected', 'revoked')) DEFAULT 'pending',"
            + "  Timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,"
            + "  FOREIGN KEY (JobID) REFERENCES Job(JobID),"
            + "  FOREIGN KEY (FreelancerID) REFERENCES Freelancer(FreelancerID)"
            + ")";
        try (Statement stmt = conn.createStatement()) {
            stmt.executeUpdate(sql);
        }
    }
    
    private static void createAdministratorTable(Connection conn) throws SQLException {
        String sql = "CREATE TABLE Administrator ("
                + "  ID INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,"
                + "  Name VARCHAR(255) NOT NULL,"
                + "  Email VARCHAR(255) NOT NULL UNIQUE,"
                + "  Password VARCHAR(255) NOT NULL"
                + ")";
        try (Statement stmt = conn.createStatement()) {
            stmt.executeUpdate(sql);
        }
    }
}
    
