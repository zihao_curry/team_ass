package EJB;

import jakarta.ejb.Local;
import Entities.freelancer;
import Entities.job;
import java.util.List;


@Local
public interface freelancerServiceLocal {
    freelancer findfreelancerByEmail(String email);
    void updateFreelancer(freelancer freelancer);
    void deleteFreelancerByEmail(String email);
    List<job> freelancershowalljobs();
    List<job> searchbyid(int ID);
    List<job> searchbykeywords(String keywords);
    List<job> freelancerallapplications(String email);
    void freelancerapplyjobs(int jobid,int fid);
    void deleteApplication(int freelancerId, int jobId);
}


