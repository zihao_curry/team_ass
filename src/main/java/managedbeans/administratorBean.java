/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSF/JSFManagedBean.java to edit this template
 */
package managedbeans;

import EJB.freelancerServiceLocal;
import EJB.providerServiceLocal;
import jakarta.inject.Named;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.context.FacesContext;
import jakarta.enterprise.context.SessionScoped;
import jakarta.inject.Inject;
import java.io.Serializable;


/**
 *
 * @author asus
 */

@Named
@SessionScoped
public class administratorBean implements Serializable {

    @Inject
    private freelancerServiceLocal freelancerService;

    private String emailToDelete;
    @Inject
    private providerServiceLocal providerService;

    private String emailToDeleteProvider;

    public String getEmailToDeleteProvider() {
        return emailToDeleteProvider;
    }

    public void setEmailToDeleteProvider(String emailToDeleteProvider) {
        this.emailToDeleteProvider = emailToDeleteProvider;
    }

    public void deleteProvider() {
        try {
            providerService.deleteProviderByEmail(emailToDeleteProvider);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Provider deleted successfully."));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Please delete all the jobs of the Provider and try again!"));
        }
    }

    public String getEmailToDelete() {
        return emailToDelete;
    }

    public void setEmailToDelete(String emailToDelete) {
        this.emailToDelete = emailToDelete;
    }

    public void deleteFreelancer() {
        try {
            freelancerService.deleteFreelancerByEmail(emailToDelete);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Freelancer deleted successfully."));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Error deleting Freelancer: " + e.getMessage()));
        }
    }
}

