package managedbeans;

import EJB.providerServiceLocal;
import Entities.job;
import Entities.jobapplication;
import Entities.provider;
import jakarta.inject.Named;
import jakarta.enterprise.context.SessionScoped;
import jakarta.inject.Inject;
import java.io.Serializable;
import java.util.List;

@Named(value = "providerBean")
@SessionScoped
public class providerBean implements Serializable {

    private int PROVIDERID;
    private String NAME;
    private String EMAIL;
    private String PASSWORD;

    private String TITLE;
    private String DESCRIPTION;
    private String STATUS;
    private String KEYWORDS;
    private Double PAYMENT_OFFER;
    private int selectedFreelancerId;
    private int selectedJobId;
    
    List<jobapplication> ja;

    // New property
    private List<job> jobs;

    @Inject
    private providerServiceLocal psl;

    // Getters and Setters
    public int getPROVIDERID() {
        return PROVIDERID;
    }

    public void setPROVIDERID(int PROVIDERID) {
        this.PROVIDERID = PROVIDERID;
    }

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public String getEMAIL() {
        return EMAIL;
    }

    public void setEMAIL(String EMAIL) {
        this.EMAIL = EMAIL;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    public String getKEYWORDS() {
        return KEYWORDS;
    }

    public void setKEYWORDS(String KEYWORDS) {
        this.KEYWORDS = KEYWORDS;
    }

    public Double getPAYMENT_OFFER() {
        return PAYMENT_OFFER;
    }

    public void setPAYMENT_OFFER(Double PAYMENT_OFFER) {
        this.PAYMENT_OFFER = PAYMENT_OFFER;
    }

    public providerServiceLocal getPsl() {
        return psl;
    }

    public void setPsl(providerServiceLocal psl) {
        this.psl = psl;
    }

    public String getPASSWORD() {
        return PASSWORD;
    }

    public void setPASSWORD(String PASSWORD) {
        this.PASSWORD = PASSWORD;
    }

    public String getTITLE() {
        return TITLE;
    }

    public void setTITLE(String TITLE) {
        this.TITLE = TITLE;
    }

    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public void setDESCRIPTION(String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }

    public int getSelectedFreelancerId() {
        return selectedFreelancerId;
    }

    public void setSelectedFreelancerId(int selectedFreelancerId) {
        this.selectedFreelancerId = selectedFreelancerId;
    }

    public int getSelectedJobId() {
        return selectedJobId;
    }

    public void setSelectedJobId(int selectedJobId) {
        this.selectedJobId = selectedJobId;
    }
    
    
//public List<jobapplication> showapplications() {
//    provider p = psl.findproviderByEmail(EMAIL);
//    ja = psl.getJobApplicationsWithMessages(p.getPROVIDERID());
//    return ja;
//}

// ...



    
    // New methods
    public List<job> getJobs() {
        return jobs;
    }

    public void setJobs(List<job> jobs) {
        this.jobs = jobs;
    }

    public void fetchJobs() {
        jobs = psl.getJobsByProvider(EMAIL);
    }

    public void publicJob() {
        psl.createJob(TITLE, DESCRIPTION, EMAIL, "open", KEYWORDS, PAYMENT_OFFER);
    }
    
    public void revokeJob(int jobId) {
        psl.revokeJob(jobId);
        fetchJobs(); // 重新获取当前用户的工作列表
    }
    
    public void saveSelectedFreelancerId(int freelancerId) {
        setSelectedFreelancerId(freelancerId);
    }
    
    public void saveSelectedJobId(int jobId) {
        setSelectedJobId(jobId);
    }
    


    
    public void acceptapp(){
        psl.acceptapplications(selectedFreelancerId, selectedJobId);
    }
    
    public void completedapp(){
        psl.completeapplications(selectedJobId,PAYMENT_OFFER,selectedFreelancerId);
    }
    
    
    
}