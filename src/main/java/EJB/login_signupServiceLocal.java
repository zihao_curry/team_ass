/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/J2EE/EJB30/SessionLocal.java to edit this template
 */
package EJB;


import jakarta.ejb.Local;

/**
 *
 * @author asus
 */
@Local
public interface login_signupServiceLocal {
    void freelancer_signup(String name,String email,String password);
    void provider_signup(String name, String email, String password);
    void administrator_signup(String name, String email, String password);
    boolean validateFreelancer(String email, String password);
    boolean validateProvider(String email, String password);
    boolean validateAdministrator(String email, String password);
}

