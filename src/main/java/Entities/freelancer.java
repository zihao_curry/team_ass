/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Entities;

/**
 *
 * @author ASUS
 */
import jakarta.persistence.*;
import java.io.Serializable;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Column;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;

@Entity
@Table(name = "FREELANCER")
public class freelancer implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "FREELANCERID")
    private int FREELANCERID;

    @NotNull
    @Pattern(regexp = "^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$", message = "Invalid name format")
    @Column(name = "NAME")

    private String NAME;

    @NotNull
    @Email
    @Column(name = "EMAIL")
    private String EMAIL;

    @NotNull
    @Pattern(regexp = "^\\d+$", message = "Password must only contain digits")
    @Column(name = "PASSWORD")
    private String PASSWORD;

    @Column(name = "SKILLS")
    private String SKILLS;

    @Column(name = "MESSAGE")
    private String MESSAGE;

    @Column(name = "PAYMENT_ACCOUNT")
    private double PAYMENT_ACCOUNT;

    public freelancer() {
    }

    public int getFREELANCERID() {
        return FREELANCERID;
    }

    public void setFREELANCERID(int FREELANCERID) {
        this.FREELANCERID = FREELANCERID;
    }

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public String getEMAIL() {
        return EMAIL;
    }

    public void setEMAIL(String EMAIL) {
        this.EMAIL = EMAIL;
    }

    public String getPASSWORD() {
        return PASSWORD;
    }

    public void setPASSWORD(String PASSWORD) {
        this.PASSWORD = PASSWORD;
    }

    public String getSKILLS() {
        return SKILLS;
    }

    public void setSKILLS(String SKILLS) {
        this.SKILLS = SKILLS;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public double getPAYMENT_ACCOUNT() {
        return PAYMENT_ACCOUNT;
    }

    public void setPAYMENT_ACCOUNT(double PAYMENT_ACCOUNT) {
        this.PAYMENT_ACCOUNT = PAYMENT_ACCOUNT;
    }


}

