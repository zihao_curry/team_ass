package EJB;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.ejb.Stateless;
import Entities.freelancer;
import Entities.provider;
import Entities.administrator;
import jakarta.persistence.NoResultException;
import jakarta.persistence.NonUniqueResultException;
import java.util.logging.Level;
import java.util.logging.Logger;

@Stateless
public class login_signupService implements login_signupServiceLocal {
    @PersistenceContext(unitName = "Junnan_LDS_war_1.0PU")
    private EntityManager em;


    
     public login_signupService() {
    }


    public void persist(Object object) {
        try {
            em.persist(object);
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void freelancer_signup(String name,String email,String password) {
        freelancer f = new freelancer();
        f.setNAME(name);
        f.setEMAIL(email);
        f.setPASSWORD(password);
        em.persist(f);
    }
    
    @Override
    public void provider_signup(String name, String email, String password){
        provider p = new provider();
        p.setNAME(name);
        p.setEMAIL(email);
        p.setPASSWORD(password);
        em.persist(p);
    }
    
    @Override
    public void administrator_signup(String name, String email, String password){
        administrator ad = new administrator();
        ad.setNAME(name);
        ad.setEMAIL(email);
        ad.setPASSWORD(password);
        em.persist(ad);
    }
    
    @Override
    public boolean validateFreelancer(String email, String password) {
        try {
            freelancer f = (freelancer) em.createQuery("SELECT f FROM freelancer f WHERE f.EMAIL = :email")
                    .setParameter("email", email)
                    .getSingleResult();
            return f != null && f.getPASSWORD().equals(password);
        } catch (NoResultException e) {
            // No freelancer with the given email was found.
            return false;
        } catch (NonUniqueResultException e) {
            // There are multiple freelancers with the same email, which shouldn't happen.
            // You should add a constraint to the database schema to prevent this situation.
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Non-unique freelancer email", e);
            return false;
        }
    }

    @Override
    public boolean validateProvider(String email, String password) {
        try {
            provider p = (provider) em.createQuery("SELECT p FROM provider p WHERE p.EMAIL = :email")
                    .setParameter("email", email)
                    .getSingleResult();
            return p != null && p.getPASSWORD().equals(password);
        } catch (NoResultException e) {
            return false;
        } catch (NonUniqueResultException e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Non-unique provider email", e);
            return false;
        }
    }

    @Override
    public boolean validateAdministrator(String email, String password) {
        try {
            administrator a = (administrator) em.createQuery("SELECT a FROM administrator a WHERE a.EMAIL = :email")
                    .setParameter("email", email)
                    .getSingleResult();
            return a != null && a.getPASSWORD().equals(password);
        } catch (NoResultException e) {
            return false;
        } catch (NonUniqueResultException e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Non-unique administrator email", e);
            return false;
        }
    }

}
