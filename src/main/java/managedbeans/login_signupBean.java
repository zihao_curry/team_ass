/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSF/JSFManagedBean.java to edit this template
 */
package managedbeans;

import jakarta.ejb.EJB;
import jakarta.inject.Named;
import EJB.login_signupServiceLocal;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.context.FacesContext;
import jakarta.validation.ConstraintViolationException;
import jakarta.enterprise.context.SessionScoped;
import jakarta.inject.Inject;
import java.io.Serializable;


/**
 *
 * @author asus
 */
@Named(value = "login_signupBean")
@SessionScoped
public class login_signupBean implements Serializable {
    
    private String name;
    private String email;
    private String password;
    private String loginEmail;
    private String loginPassword;
    @Inject
    private freelancerBean freelancerBean;
    @Inject
    private providerBean providerBean;
    


    @EJB
    private login_signupServiceLocal lsl;

public String freelancersignup() {
    try {
        lsl.freelancer_signup(name, email, password);
        return "registrationSuccess";
    } catch (ConstraintViolationException e) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Validation Error", e.getMessage()));
        return "";
    }
}

public String providersignup() {
    try {
        lsl.provider_signup(name, email, password);
        return "registrationSuccess";
    } catch (ConstraintViolationException e) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Validation Error", e.getMessage()));
        return "";
    }
}

public String administratorsignup() {
    try {
        lsl.administrator_signup(name, email, password);
        return "registrationSuccess";
    } catch (ConstraintViolationException e) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Validation Error", e.getMessage()));
        return "";
    }
}


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public login_signupBean(){
    
    }
    
    public String getLoginEmail() {
        return loginEmail;
    }

    public void setLoginEmail(String loginEmail) {
        this.loginEmail = loginEmail;
    }

    public String getLoginPassword() {
        return loginPassword;
    }

    public void setLoginPassword(String loginPassword) {
        this.loginPassword = loginPassword;
    }
    
    // Add the login method
    public String freelancerLogin() {
        if (lsl.validateFreelancer(loginEmail, loginPassword)) {
            freelancerBean.setEmail(loginEmail);
            return "freelancerMain";
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Invalid email or password", null));
            return null; // 保持在当前页面
        }
    }
    
    public String providerLogin() {
        if (lsl.validateProvider(loginEmail, loginPassword)) {
            providerBean.setEMAIL(loginEmail);
            return "providerMain";
        } else {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Invalid email or password", null);
            FacesContext.getCurrentInstance().addMessage(null, message);
            return null;
        }
    }

    public String administratorLogin() {
        if (lsl.validateAdministrator(loginEmail, loginPassword)) {
            return "administratorMain";
        } else {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Invalid email or password", null);
            FacesContext.getCurrentInstance().addMessage(null, message);
            return null;
        }
    }

 
}
